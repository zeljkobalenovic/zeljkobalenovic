(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserController', UserController);

    UserController.$inject = ['$http'];

    function UserController($http) {
        var vm = this;

        vm.user = [];
        vm.getAll = getAll;
        vm.getByCompname = getByCompname;
        vm.deleteUser = deleteUser;

        init();

        function init(){
            getAll();
        }

        function getAll(){
            var url = "/user/all";
            var userPromise = $http.get(url);
            userPromise.then(function(response){
                vm.user = response.data;
            });
        }

        function getByCompname(){
            var url = "/user/bycompname/" + "Hl"
            var userPromise = $http.get(url);
            userPromise.then(function(response){
                vm.user = response.data;
            });
        }

        function deleteUser(id){
            var url = "/user/delete/" + id;
            $http.post(url).then(function(response){
                vm.user = response.data;
            });
        }
    }
})();