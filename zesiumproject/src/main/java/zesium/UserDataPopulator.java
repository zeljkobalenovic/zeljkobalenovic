package zesium;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class UserDataPopulator implements CommandLineRunner{

	
	private UserRepository userRepository ;
	
	
	@Autowired
	public UserDataPopulator(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		
		List<User> users = new ArrayList <> ();
		
		users.add(new User(4,"Petar Petrovic" , "pera@gmail.com" , "pera123" , "123456" , "Hleb A.D." , 23));
		users.add(new User(10,"Milan Peric" ,  "miki@yahoo.com" ,"miki123" , "123456" , "Neoplanta A.D." , 25));
		users.add(new User(11,"Lidija Jokic" , "lidija@mercator.rs" , "lidija123" , "123456" , "Mercator Beograd" , 27));
		userRepository.saveAll(users);
	
	}

}
