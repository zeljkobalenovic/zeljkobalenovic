package zesium;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")

public class UserController {

	private UserRepository userRepository ;
	
	
	
	public UserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<User> getAll () {
		return userRepository.findAll() ;
	}
	
	@RequestMapping(value="/byorgnmb/{orgnmb}" , method=RequestMethod.GET)
	public List<User> filterByOrganizationNumber (@PathVariable long orgnmb) {
	//	return users.stream().filter(x -> x.getOrganizationNumber() == orgnmb).collect(Collectors.toList());	varijanta 1 bez repozitorija i baze
		return userRepository.findByOrganizationNumber(orgnmb) ;
	}
	
	@RequestMapping(value="/bycompname/{compname}" , method=RequestMethod.GET)
	public List<User> filterByCompanyName (@PathVariable String compname) {
	//	return users.stream().filter(x -> x.getCompanyName().equalsIgnoreCase(compname)).collect(Collectors.toList());
		return userRepository.findByCompanyNameContaining(compname) ;
	}
	
	@RequestMapping(value="/create" , method=RequestMethod.POST)
	public List<User> createUser ( @RequestBody User user) {
	//	users.add(user) ;
	//	return users;
		userRepository.save(user) ;
		return userRepository.findAll() ;
	}
	
	@RequestMapping(value="/delete/{id}" , method=RequestMethod.POST)
	public List<User> deleteUser (@PathVariable long id) {
		userRepository.deleteById(id);
		return userRepository.findAll() ;
		
		
	}
}
