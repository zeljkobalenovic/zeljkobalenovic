package zesium;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZesiumprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZesiumprojectApplication.class, args);
	}

}

// nesto menjam