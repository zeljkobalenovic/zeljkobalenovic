package zesium;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id ;
	private String nameAndSurname ;
	private String email ;
	private String password ;
	private String phoneNumber ;
	private String companyName ;
	private long organizationNumber ;
	

	public User() {}
	
	public User(long id, String nameAndSurname, String email , String password, String phoneNumber, String companyName, long organizationNumber) {
		this.id = id;
		this.nameAndSurname = nameAndSurname;
		this.email = email;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.companyName = companyName;
		this.organizationNumber = organizationNumber;
	}

	public long getId() {
		return id;
	}

	public String getNameAndSurname() {
		return nameAndSurname;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public long getOrganizationNumber() {
		return organizationNumber;
	}
	
	
	
	
}
