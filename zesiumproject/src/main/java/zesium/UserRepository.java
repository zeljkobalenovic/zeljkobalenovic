package zesium;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

		List <User> findByOrganizationNumber(long orgnmb) ;
		List <User> findByCompanyNameContaining(String compname) ;
}
